<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Session;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function select(Request $data)
    {

        $email = $data->email;
        $password = $data->password;

        $user_favorites = User::where('email', $email)->first();
        if($user_favorites != null){
            if($password == $user_favorites->password){
                Auth::login($user_favorites, TRUE);
                return redirect('/admin');
            }else{
                return redirect()->back()->with('alertlogin','Password Salah !');
            }
        }else{
            return redirect()->back()->with('alertlogin','User tidak terdaftar');
        }

    }

    public function logout(Request $request) {
      Auth::logout();
      return redirect('/');
    }

    public function alertlogin()
    {
        return view('auth.login');
    }


}
