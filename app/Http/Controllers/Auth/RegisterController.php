<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'rekomendator' => ['required', 'string', 'max:255'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $data)
    {

        $rules = [
            'name' => 'required|string',
            'phone' => 'required|Numeric|digits_between : 10, 13|unique:users,phone',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:5',
            'conpassword' => 'same:password',
            'rekomendator' => 'required',
        ];
        $data->validate($rules);

        $affected = User::create(array(
             'nama_user' => $data->name, 
             'phone' => $data->phone, 
             'email' => $data->email,
             'password' => $data->password, 
             'kode_referral' => $data->rekomendator
        ));

        if ($affected) {
            return view('auth.alertlogin');
        } else {
            return 'failed';
        }   
    }

    public function register()
    {
        return view('auth.register');
    }


}
