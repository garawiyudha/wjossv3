<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;

class ProductController extends Controller
{
    public function getproduk() {
    	//$products = $art->products->skip(0)->take(10)->get(); //get first 10 rows
    	$data = product::where('id_kategori', '65')->take(4)->get(); 
    	$data2 = product::where('id_kategori', '104')->take(4)->get(); //pria
    	$data3 = product::where('id_kategori', '97')->take(4)->get(); //wanita
    	$data4 = product::where('id_kategori', '133')->take(4)->get(); //anak
    	$data5 = product::where('id_kategori', '123')->take(4)->get(); //umum
    	
    	return view('index')->with('produk', $data)->with('produk2', $data2)->with('produk3', $data3)->with('produk4', $data4)->with('produk5', $data5);

    }


    public function detail($produkId)
    {
    	$data = product::where('id_produk', $produkId)->first();
    	// $data2 = product::where('id_kategori', '118')->take(4)->get();
    	$data2 = product::where('id_kategori', $data->id_kategori)->where('id_produk', '!=', $data->id_produk)->take(4)->get();
    
    	return view('detail')->with('detail', $data)->with('featured', $data2);
    }
}
