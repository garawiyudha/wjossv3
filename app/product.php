<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_produk
 * @property int $id_toko
 * @property int $id_kategori
 * @property int $id_stok_status
 * @property string $kode_produk
 * @property string $nama_produk
 * @property float $harga
 * @property int $jumlah_stok
 * @property string $deskripsi
 * @property string $tag
 * @property string $date_add
 * @property string $image
 * @property Toko $toko
 * @property Kategori $kategori
 * @property StokStatus $stokStatus
 * @property Toko[] $tokos
 * @property TransaksiDetail[] $transaksiDetails
 */
class product extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'produk';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_produk';

    /**
     * @var array
     */
    protected $fillable = ['id_toko', 'id_kategori', 'id_stok_status', 'kode_produk', 'nama_produk', 'harga', 'jumlah_stok', 'deskripsi', 'tag', 'date_add', 'image'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function toko()
    {
        return $this->belongsTo('App\Toko', 'id_toko', 'id_toko');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'id_kategori', 'id_kategori');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stokStatus()
    {
        return $this->belongsTo('App\StokStatus', 'id_stok_status', 'id_stok_status');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tokos()
    {
        return $this->belongsToMany('App\Toko', 'toko_detail', 'id_produk', 'id_toko');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transaksiDetails()
    {
        return $this->hasMany('App\TransaksiDetail', 'id_produk', 'id_produk');
    }
}
