<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nama_user', 'email', 'email_verified_at', 'password', 'remember_token', 'created_at', 'updated_at', 'kode_referral', 'id_gender', 'provinsi', 'kota/kabupaten', 'alamat', 'phone'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gender()
    {
        return $this->belongsTo('App\Gender', '"id_gender"', '"id_gender"');
    }
}
