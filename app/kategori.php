<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id_Kategori
 * @property string $Nama_Kategori
 * @property Produk[] $produks
 */
class kategori extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'kategori';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_kategori';

    /**
     * @var array
     */
    protected $fillable = ['nama_kategori'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function produks()
    {
        return $this->hasMany('App\Produk', '"id_kategori"', '"id_kategori"');
    }
}
