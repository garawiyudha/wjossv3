<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("[data-toggle=popover]").each(function(i, obj) {
	$(this).popover({
  	html: true,
  	content: function() {
    var id = $(this).attr('id')
    return $('#popover-content-' + id).html();
  	}
});

});  
});

</script>
<style>
.popover {max-width:400px;}

#popover-content-logout > * {
  background-color:#ff0000 !important;
}
</style>
<div class="header">
	<div class="container">
		<ul>
		    <li> <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-unlock-alt" aria-hidden="true"></i> 
            <span class="txt-head-cont">Sign In</span></a></li>
			<li> <a href="#" data-toggle="modal" data-target="#myModal2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
            <span class="txt-head-cont">Sign Up</span></a></li>
			<li><i class="fa fa-phone" aria-hidden="true"></i> 
            <span class="txt-head-cont">Call : 01234567898</span></li>
			<li><button style="border: none; background: none;" data-toggle="popover" data-trigger="focus" data-container="body" data-placement="top" type="button" data-html="true" id="language"><i class="fa fa-language" aria-hidden="true"></i> 
            <span class="txt-head-cont">Language</span></button></li>
		</ul>
<div id="popover-content-language" class="hidden">
<div style="margin-bottom:10px;"><small>Pilih Bahasa</small></div>
<ul class="list-group" style="margin-bottom: 0px; text-align:center;">
  <li class="list-group-item"><a href="#" title="English"><img src="images/en-gb.png" /> <span class="txt-head-cont"> English</span></a></li>
  <li class="list-group-item"><a href="#" title="Indonesia"><img src="images/id-id.png" /> <span class="txt-head-cont">Indonesia</span></a></li>  
</ul>
</div>
	</div>
</div>
