<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

 });

Route::get('/testing', 'ProductController@getproduk');

Route::get('/detail/{produkId}', 'ProductController@detail');

Route::get('/about', function ()
{
	return view('about');
});

Route::get('/contact', function ()
{
	return view('contact');
});

Route::get('/categories', function ()
{
	return view('categories');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('logout', 'Auth\LoginController@logout');

Route::get('/registerwjoss', 'Auth\RegisterController@register');

Route::post('/registerwjoss', 'Auth\RegisterController@create');

Route::post('/loginwjoss', 'Auth\LoginController@select');

Route::get('/loginwjoss', 'Auth\LoginController@alertlogin');

Route::post('/status', 'Auth\LoginController@select');

Route::get('/loginkembali', 'Auth\LoginController@alertlogin');

Auth::routes();

Route::get('/admin', 'AdminLteController@index');

Route::post('/send/email', 'HomeController@mail');

Route::get('/reset', 'HomeController@reset');

Auth::routes(['verify' => true]);

// Route::get('/update', 'HomeController@update');
//Route::get('/register', 'RegistrationController@create');
//Route::post('/regist', 'RegistrationController@register');
//Route::get('/helloworld', 'HelloWorldController@index');
//Route::get('/pegawai','PegawaiController@index');
//Route::get('/pegawai/tambah','PegawaiController@tambah');
//Route::post('/pegawai/store','PegawaiController@store');
//Route::get('/pegawai/edit/{id}','PegawaiController@edit');
//Route::post('/pegawai/update','PegawaiController@update');
//Route::get('/pegawai/hapus/{id}','PegawaiController@hapus');
//Route::get('/home', 'CobaController@index')->name('home');
//Route::get('ajaxRequest', 'CobaController@ajaxRequest');
//Route::post('ajaxRequest', 'CobaController@ajaxRequestPost');