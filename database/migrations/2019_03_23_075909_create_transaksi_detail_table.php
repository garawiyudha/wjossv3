<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransaksiDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaksi_detail', function(Blueprint $table)
		{
			$table->integer('Id_Transaksi');
			$table->integer('Id_Produk');
			$table->integer('Qty');
			$table->decimal('Subtotal', 10, 0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaksi_detail');
	}

}
