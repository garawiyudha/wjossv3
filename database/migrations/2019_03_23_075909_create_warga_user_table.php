<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWargaUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('warga_user', function(Blueprint $table)
		{
			$table->integer('Id_User');
			$table->integer('Id_KK');
			$table->integer('NIK');
			$table->integer('Id_Hubungan_Keluarga');
			$table->integer('Id_Agama');
			$table->string('Tempat_Lahir', 1);
			$table->dateTime('Tanggal_Lahir');
			$table->integer('Id_Pendidikan_Terakhir');
			$table->string('Pekerjaan');
			$table->integer('Id_Kewarganegaraan');
			$table->integer('Id_Status_Pernikahan');
			$table->integer('Id_Golongan_Darah');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('warga_user');
	}

}
