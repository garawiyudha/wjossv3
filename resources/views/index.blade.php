@extends('layouts.dashboard')

@section('content')

@include('layouts.slider')
@include('layouts.categories')

<div class="new_arrivals_agile_w3ls_info"> 
		<div class="container">

		    <h3 class="wthree_text_info">POPULER <span>Bulan ini</span></h3>
            <div class="prod-list">

            	@foreach($produk as $p)
            	<div class="col-md-3 product-men">
					<div class="men-pro-item simpleCart_shelfItem">
						<div class="men-thumb-item">
							<img src="{{asset($p->image)}}" alt="" class="pro-image-front">
							<img src="{{asset($p->image)}}" alt="" class="pro-image-back">
								<div class="men-cart-pro">
									<div class="inner-men-cart-pro">
										<a href="{{URL('/detail/'.$p->id_produk )}}" class="link-product-add-cart">Quick View</a>
									</div>
								</div>
								<span class="product-new-top">New</span>
											
						</div>
						<div class="item-info-product ">
							<h4 class="ttl-prod"><a href="{{URL('/detail/'.$p->id_produk )}}">{{$p->nama_produk}}</a></h4>
							<div class="info-product-price">
								<span class="item_price">Rp. {{ number_format($p->harga, 2, ',', '.') }}</span>
								<!-- <del>{{$p->harga}}</del> -->
							</div>
							<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
									<form action="#" method="post">
										<fieldset>
											<input type="hidden" name="cmd" value="_cart" />
											<input type="hidden" name="add" value="1" />
											<input type="hidden" name="business" value=" " />
											
											<input type="hidden" name="item_name" value='{{$p->nama_produk}}'/>
											<input type="hidden" name="amount" value='{{$p->harga}}'/>
											<input type="hidden" name="discount_amount" value="" />
											<input type="hidden" name="currency_code" value="IDR" />
											<input type="hidden" name="return" value=" " />
											<input type="hidden" name="cancel_return" value=" " />
											<input type="submit" name="submit" value="Add to cart" class="button" />
										</fieldset>
									</form>
							</div>											
						</div>
					</div>
				</div>
				@endforeach

			</div>
        </div>
        @include('layouts.pagination')
</div>
@include('recommended-products')
<!-- @include('discounted-products') -->

<!-- <div class="banner-bootom-w3-agileits">
	<div class="container">
	

	<h3 class="wthree_text_info">Apa <span>Tren-nya?</span></h3>
	
		<div class="col-md-5 bb-grids bb-left-agileits-w3layouts">
			<a href="categories.php">
			   <div class="bb-left-agileits-w3layouts-inner grid">
					<figure class="effect-roxy">
							<img src="template/images/bb1.jpg" alt=" " class="img-responsive" />
							<figcaption>
								<h3><span>S</span>ale </h3>
								<p>Upto 55%</p>
							</figcaption>			
						</figure>
			    </div>
			</a>
		</div>
		<div class="col-md-7 bb-grids bb-middle-agileits-w3layouts">
		       <div class="bb-middle-agileits-w3layouts grid">
			           <figure class="effect-roxy">
							<img src="template/images/bottom3.jpg" alt=" " class="img-responsive" />
							<figcaption>
								<h3><span>S</span>ale </h3>
								<p>Upto 55%</p>
							</figcaption>			
						</figure>
		        </div>
		      <div class="bb-middle-agileits-w3layouts forth grid">
						<figure class="effect-roxy">
							<img src="template/images/bottom4.jpg" alt=" " class="img-responsive">
							<figcaption>
								<h3><span>S</span>ale </h3>
								<p>Upto 65%</p>
							</figcaption>		
						</figure>
					</div>
		<div class="clearfix"></div>
	</div>
    </div>
</div> -->

<!-- <div class="agile_last_double_sectionw3ls">
            <div class="col-md-6 multi-gd-img multi-gd-text ">
					<a href="wocategories.php"><img src="template/images/bot_1.jpg" alt=" "><h4>Flat <span>50%</span> offer</h4></a>
					
			</div>
			 <div class="col-md-6 multi-gd-img multi-gd-text ">
					<a href="wocategories.php"><img src="template/images/bot_2.jpg" alt=" "><h4>Flat <span>50%</span> offer</h4></a>
			</div>
			<div class="clearfix"></div>
</div> -->
@include('layouts.unique')
<div class="sale-w3ls">
			<div class="container">
				<h6>We Offer Flat <span>40%</span> Discount</h6>
 
				<a class="hvr-outline-out button2" href="single.php">Shop Now </a>
			</div>
		</div>

@endsection