@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>


               
                <div class="card-body">
                    <form method="post" action="/registerwjoss">
                        @csrf
                    <p>BACA PETUNJUK SEBELUM INPUT DATA ANDA</p>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                         <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required autofocus>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                            <input type="checkbox" onclick="showpass()">Show Password
                            <script>
                            function showpass() {
                              var x = document.getElementById("password");
                              if (x.type === "password") {
                                x.type = "text";
                              } else {
                                x.type = "password";
                              }
                            }
                            </script>
                            
                            </div>
                        </div>    

                        <div class="form-group row">
                            <label for="rekomendator" class="col-md-4 col-form-label text-md-right">{{ __('Rekomendator') }}</label>

                            <div class="col-md-6">
                                <input id="rekomendator" type="number" class="form-control{{ $errors->has('rekomendator') ? ' is-invalid' : '' }}" name="rekomendator" value="{{ old('rekomendator') }}" required autofocus>

                                @if ($errors->has('rekomendator'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('rekomendator') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="Reset" class="btn btn-primary">
                                    {{ __('Reset') }}
                                    
                                </button>

                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>

                                </div>
                        </div>


                  <!--      <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div> -->

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- <script type="text/javascript">



    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });



    $(".btn-primary").click(function(e){

        e.preventDefault();



        var name = $("input[name=Name]").val();
        var name = $("input[name=Phone]").val();
        var email = $("input[name=Email]").val();
        var email = $("input[name=Username").val();
        var password = $("input[name=Password]").val();
        var email = $("input[name=Rekomendator]").val();

  


        $.ajax({

           type:'POST',

           url:'/ajaxRequest',

           data:{name:name, phone:phone, email:email, password:password, email:email, rekomendator:rekomendator},

           success:function(data){

              alert(data.success);

           }

        });



    });

</script> -->
@endsection
