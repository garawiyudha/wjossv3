<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta property="og:site_name" content="Marketplace Wjoss | Jual Beli Jujur, simple, mudah, dan aman" />
<meta property="og:locale" content="en_us" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Marketplace Wjoss | Jual Beli Jujur, simple, mudah, dan aman" />
<meta property="og:url" content="https://wjoss.com" />
<meta property="og:description" content="Layanan untuk JUAL BELI JUJUR warga, layaknya ecommerce atau toko online. Layaknya sosial media, layaknya sistem untuk me-support warga dalam menjual produk-produk mereka, tempat promo usaha jasa apapun, dan merupakan marketplace produk-produk umum, baik baru maupun bekas. Juga terdapat aplikasi RT RW untuk membantu sistem administrasi pendataan warga tingkat RT." />
<meta property="og:image" content="images/wjoss-og-icon.jpg" />
<meta property="og:image:alt" content="images/wjoss-og-icon.jpg" />
<title>Marketplace Wjoss | Jual Beli Jujur, simple, mudah, dan aman</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="wjoss, warga joss, jual beli jujur, toko online warga, toko online umkm, ecommers indonesia, toko online murah, aplikasi rt rw" />
<meta name="description" content="Disebut sebagai WJOSS. Suatu layanan online untuk Warga Indonesia, dengan kepanjangan kata adalah WARGA JOSS!. Layanan untuk JUAL BELI JUJUR warga, layaknya ecommers atau toko online atau online shop. Layaknya sosial media, layaknya sistem untuk me-support warga dalam menjual produk-produk mereka, suatu media untuk mempopularitaskan UMKM, tempat promo usaha jasa apapun, atau merupakan marketplace produk-produk umum, baik baru maupun bekas. Juga terdapat aplikasi RT RW untuk membantu sistem administrasi pendataan warga tingkat RT." />
<link href="{{asset('template/images/favicon.png')}}" rel="icon" />
<meta name="theme-color" content="#015856"> <!-- Untuk Windows Phone --> 
<meta name="msapplication-navbutton-color" content="#015856"> <!-- Untuk Safari iOS --> 
<meta name="apple-mobile-web-app-capable" content="yes"> 
<meta name="apple-mobile-web-app-status-bar-style" content="#015856">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<meta name="revisit-after" content="3 days" />
<meta http-equiv="reply-to" content=joss@jawaracorpo.com" />
<meta name="distribution" content="global" />
<link href="{{asset('template/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset('template/css/flexslider.css')}}" type="text/css" media="screen" />
<link href="{{asset('template/css/font-awesome.css')}}" rel="stylesheet"> 
<link href="{{asset('template/css/easy-responsive-tabs.css')}}" rel='stylesheet' type='text/css'/>
<link href="{{asset('template/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('template/css/jquery-ui.css')}}">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic" rel='stylesheet' type='text/css'>

