<!DOCTYPE html>
<html>
    <head>
    	@include('layouts.header')
    </head>
    <body>
    <div id="home"></div>
	@include('layouts.bottom-bar')
	@include('layouts.top-bar')

   	@yield('content')
    	
	@include('layouts.footer')
    @if(session('alertlogin'))
    <script type="text/javascript">
        $('#myModal').modal('show');
    </script>
    @endif

    @if ($errors->any())
        <script type="text/javascript">
        $('#myModal2').modal('show');
        </script>
    @endif
	</body>
</html>
