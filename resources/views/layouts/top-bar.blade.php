<div class="ban-top">
	<div class="container">
		<div class="top_nav_left">
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav menu__list">                  	
                  	<li class=" menu__item" ><a class="menu__link" onclick="history.go(-1);">
                    <span class="glyphicon glyphicon-chevron-left"></span></a></li>
					<li class="active menu__item menu__item--current"><a class="menu__link" href="{{URL('/testing')}}" style="padding: 10px 0px;">
                    <img src="{{asset('template/images/logo.png')}}" style="width:100px;"> <span class="sr-only">(current)</span></a></li>
					<li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kategori<span class="caret"></span></a>
							<ul class="dropdown-menu multi-column columns-3">
								<div class="agile_inner_drop_nav_info">
									<div class="col-sm-6 multi-gd-img1 multi-gd-text ">
										<a href="{{URL('/categories')}}"><img src="{{asset('template/images/top2.jpg')}}" alt=" "/></a>
									</div>
									<div class="col-sm-3 multi-gd-img">
										<ul class="multi-column-dropdown">
											<li><a href="{{URL('/categories')}}">Clothing</a></li>
											<li><a href="{{URL('/categories')}}">Wallets</a></li>
											<li><a href="{{URL('/categories')}}">Footwear</a></li>
											<li><a href="{{URL('/categories')}}">Watches</a></li>
											<li><a href="{{URL('/categories')}}">Accessories</a></li>
											<li><a href="{{URL('/categories')}}">Bags</a></li>
											<li><a href="{{URL('/categories')}}">Caps & Hats</a></li>
										</ul>
									</div>
									<div class="col-sm-3 multi-gd-img">
										<ul class="multi-column-dropdown">
											<li><a href="{{URL('/categories')}}">Jewellery</a></li>
											<li><a href="{{URL('/categories')}}">Sunglasses</a></li>
											<li><a href="{{URL('/categories')}}">Perfumes</a></li>
											<li><a href="{{URL('/categories')}}">Beauty</a></li>
											<li><a href="{{URL('/categories')}}">Shirts</a></li>
											<li><a href="{{URL('/categories')}}">Sunglasses</a></li>
											<li><a href="{{URL('/categories')}}">Swimwear</a></li>
										</ul>
									</div>
									<div class="clearfix"></div>
								</div>
							</ul>
					</li>
                    <li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Layanan <span class="caret"></span></a>
							<ul class="dropdown-menu multi-column columns-3">
								<div class="agile_inner_drop_nav_info">
									<div class="col-sm-3 multi-gd-img">
										<ul class="multi-column-dropdown">                                        	
											<li><a href="blog.php">Blog</a></li>
                                            <li><a href="#">Wjapp Chat Application</a></li>
											<li><a href="#">Wjoss Social Application</a></li>
                                            <li><a href="#">Wjoss Ecommerce</a></li>											
										</ul>
									</div>
									<div class="col-sm-3 multi-gd-img">
										<ul class="multi-column-dropdown">
											<li><a href="#" title="Sistem input data warga yang bisa di edit, hapus, dan simpan, dari data personal sampai KK, yang tersimpan dengan aman">Input Data Warga</a></li>
											<li><a href="#" title="Sistem untuk pengurus RT yang memudahkan dalam proses pembuatan surat-surat keterangan yang diminta warga, dengan sekali klik, surat sudah siap cetak">Penerbitan Surat Keterangan Warga</a></li>
											<li><a href="#" title="Pembuatan segala kegiatan warga, dari kegiatan rutin bulanan, seperti arisan warga, jadwal kerja bakti, penyelenggaraan peringatan hari-hari besar, jadwal pos kamling, posyandu, dan lain-lain, beserta struktur kepanitiaan dan budgeting">Atur Kegiatan Warga</a></li>
											<li><a href="#" title="Sistem Vote sederhana untuk membantu warga dalam mengenal leader dan calon leader yang dipilih menjadi kandidat di tingkat RT, RW, Kelurahan, Kecamatan, sampai Kursi Presiden">Sistem Pemilu RT s/d Presiden </a></li>
											<li><a href="#" title="Mesin Pencari Hirarki/ Geneologi/ Struktur Keluarga, dari orangtua, kakek-nenek, dan seterusnya.">Sistem Susunan Anggota Keluarga</a></li>
											<li><a href="#" title="Kumpulan Arsip berbagai kegiatan warga, sebagai alat popularitas karya-karya original warga.">Blog Kegiatan Warga </a></li>
											<li><a href="#" title="Layanan untuk Kemudahan warga dalam memanage usahanya, baik toko online maupun toko offline (under develop)">Mesin Kasir Warga</a></li>
                                            <li><a href="#" title="Layanan untuk warga dalam kemudahan mem-publish potensi usahanya, Sumber Daya Alamnya, Sumber Daya Manusianya, untuk kebutuhan validitas data prestasi peningkatan kemampuan swadaya masyarakat.">Support System SDA - SDM</a></li>
										</ul>
									</div>
									<div class="col-sm-6 multi-gd-img multi-gd-text ">
										<a href="#"><img src="{{asset('template/images/wjoss-og-icon.jpg')}}" alt="Marketplace Wjoss"/></a>
									</div>
									<div class="clearfix"></div>
								</div>
							</ul>
					</li>
                    <li class=" menu__item"><a class="menu__link" href="{{URL('/about')}}">Tentang</a></li>                    
					<li class=" menu__item"><a class="menu__link" href="{{URL('/contact')}}">Kontak</a></li>
				  </ul>
				</div>
			  </div>
			</nav>	
		</div>
		<div class="top_nav_right">
			<div class="wthreecartaits wthreecartaits2 cart cart box_1"> 
						<form action="#" method="post" class="last"> 
						<input type="hidden" name="cmd" value="_cart">
						<input type="hidden" name="display" value="1">
						<button class="w3view-cart" type="submit" name="submit" value="">
                        <img src="{{asset('template/images/favicon.png')}}" aria-hidden="true">
                        </button>
					</form>  
  
						</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
						<div class="modal-body modal-body-sub_agile">
						<div class="col-md-8 modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Sign In <span>Now</span></h3>
						<form action="/loginwjoss" method="post">
							@csrf
							<div class="styled-input">
								<input id="email" input type="email" name="email" value="{{ old('email') }}" required autofocus> 
							<label>Email</label>
								<span></span>
							</div> 

							<div class="styled-input agile-styled-input-top">
								<input id="password" input type="password" name="password" value="{{ old('password') }}" required autofocus>
								<label>Password</label>
								<span></span>
							</div>

							<input type="submit" value="Sign In">

								@if (session('alertlogin'))
								<script>
									$('#myModal').modal('show');
								</script>
								    <div class="alert alert-danger">
								        {{ session('alertlogin') }}
								    </div>
								@endif

						</form>


						  <ul class="social-nav model-3d-0 footer-social w3_agile_social top_agile_third">
															<li><a href="#" class="facebook">
																  <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="twitter"> 
																  <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="instagram">
																  <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="pinterest">
																  <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
														</ul>
														<div class="clearfix"></div>
														<p><a href="#" data-toggle="modal" data-target="#myModal2" > Don't have an account?</a></p>

						</div>
						<div class="col-md-4 modal_body_right modal_body_right1"  style="background-image:url({{asset('template/images/wjoss-login.jpg')}}); background-position:right; background-repeat:no-repeat; height:430px;">
							
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- //Modal content-->
			</div>
		</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
						<div class="modal-body modal-body-sub_agile">
						<div class="col-md-8 modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Sign Up <span>Now</span></h3>
						 <form action="/registerwjoss" method="post">
						 	@csrf
							<div class="styled-input agile-styled-input-top">
								<input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus>
                                
								<label>Name</label>
								<span></span>
							</div>
							@if ($errors->has('name'))
                                   <div class="alert alert-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                            @endif
							<div class="styled-input">
								<input id="phone" type="text" name="phone" value="{{ old('phone') }}" required autofocus> 
								<label>Phone</label>
								<span></span>
							</div> 
							@if ($errors->has('phone'))
                                    <div class="alert alert-danger">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </div>
                                @endif
							<div class="styled-input">
								<!-- @if (session('alert'))
								    <div class="alert alert-success">
								        {{ session('alert') }}
								    </div>
								@endif -->
								<input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>

                               
								<label>Email</label>
								<span></span>
							</div> 
							 @if ($errors->has('email'))
                                    <div class="alert alert-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif 
							<div class="styled-input">
								<input id="password1" type="password" name="password" value="{{ old('password1') }}" required autofocus>

								<label>Password</label>
								<span></span>
							</div>

                                @if ($errors->has('password'))
                                    <div class="alert alert-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif 
							<div class="styled-input">
								<input id="conpassword" type="password" name="conpassword" value="{{ old('conpassword') }}" required autofocus> 
								<label>Confirm Password</label>
								<span></span>
							</div> 
							@if ($errors->has('conpassword'))
                                    <div class="alert alert-danger">
                                        <strong>{{ $errors->first('conpassword') }}</strong>
                                    </div>
                                @endif
							<div>
								<input type="checkbox" onclick="showpass()"> Show Password
                            <script>
                            function showpass() {
                              var x = document.getElementById("password1");
                              var y = document.getElementById("conpassword");
                              if (x.type === "password") {
                                x.type = "text";
                                y.type = "text";
                              } else {
                                x.type = "password";
                                y.type = "password";
                              }
                            }
                            </script>
							</div>
							<div class="styled-input">
								<input id="rekomendator" type="text" name="rekomendator" value="{{ old('rekomendator') }}" required autofocus>

                                @if ($errors->has('rekomendator'))
                                   <div class="alert alert-danger">
                                        <strong>{{ $errors->first('rekomendator') }}</strong>
                                    </div>
                                @endif
                                <label>Kode Refferal</label>
								<span></span>
							</div> 
							<input type="submit" value="Sign Up">
						</form>
						  <ul class="social-nav model-3d-0 footer-social w3_agile_social top_agile_third">
															<li><a href="#" class="facebook">
																  <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="twitter"> 
																  <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="instagram">
																  <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="pinterest">
																  <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
														</ul>
														<div class="clearfix"></div>
														<p><a href="#">By clicking register, I agree to your terms</a></p>

						</div>
						<div class="col-md-4 modal_body_right modal_body_right1" style="background-image:url({{asset('template/images/wjoss-welcome.jpg')}}); background-position:right; background-repeat:no-repeat; height:430px;">							
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- //Modal content-->
			</div>
		</div>		
