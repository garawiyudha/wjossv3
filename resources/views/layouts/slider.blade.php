<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1" class=""></li>
			<li data-target="#myCarousel" data-slide-to="2" class=""></li>
			<li data-target="#myCarousel" data-slide-to="3" class=""></li>
			<li data-target="#myCarousel" data-slide-to="4" class=""></li> 
		</ol>
		<div class="carousel-inner" role="listbox">
			<div class="item active"> 
				<div class="container">
					<div class="carousel-caption">
						<h3>Hadir dengan <span>Unik</span></h3>
						<p>Kombinasi Marketplace dan Administrasi Warga</p>
						<a class="hvr-outline-out button2" href="#">Daftar Sekarang</a>
					</div>
				</div>
			</div>
			<div class="item item2"> 
				<div class="container">
					<div class="carousel-caption">
						<h3>Siap menjadi <span>Trend Setter</span></h3>
						<p>Sistem pendataan yang <strong>VALID</strong></p>
						<a class="hvr-outline-out button2" href="#">Daftar Sekarang</a>
					</div>
				</div>
			</div>
			<div class="item item3"> 
				<div class="container">
					<div class="carousel-caption">
						<h3>Ada administrasi <span>Warga</span></h3>
						<p>Mendukung kemudahan pendataan</p>
						<a class="hvr-outline-out button2" href="#">Daftar Sekarang</a>
					</div>
				</div>
			</div>
			<div class="item item4"> 
				<div class="container">
					<div class="carousel-caption">
						<h3>Marketplace <span>Syariah</span></h3>
						<p>Fee sistem yang Jujur</p>
						<a class="hvr-outline-out button2" href="categories.php">Belanja Sekarang</a>
					</div>
				</div>
			</div>
			<div class="item item5"> 
				<div class="container">
					<div class="carousel-caption">
						<h3>Bermacam-macam <span>Koleksi</span></h3>
						<p>Murah, lengkap, dan Aman</p>
						<a class="hvr-outline-out button2" href="categories.php">Belanja Sekarang</a>
					</div>
				</div>
			</div> 
		</div>
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
		<!-- The Modal -->
    </div> 