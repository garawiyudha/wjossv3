<div class="new_arrivals_agile_w3ls_info"> 
		<div class="container">
		    <h3 class="wthree_text_info">PRODUK</h3>		
				<div id="horizontalTab">
						<ul class="resp-tabs-list">
							<li> Pria</li>
							<li> Wanita</li>
							<li> Anak</li>
							<li> Sepatu & Sandal</li>
						</ul>
					<div class="resp-tabs-container">
					<!--/tab_one-->
						<div class="tab1">
							@foreach($produk2 as $p2) 
							<div class="col-md-3 product-men">
								<div class="men-pro-item simpleCart_shelfItem">
									<div class="men-thumb-item">
										<img src="{{$p2->image}}" alt="" class="pro-image-front">
										<img src="{{$p2->image}}" alt="" class="pro-image-back">
											<div class="men-cart-pro">
												<div class="inner-men-cart-pro">
													<a href="{{URL('/detail/'.$p2->id_produk )}}" class="link-product-add-cart">Quick View</a>
												</div>
											</div>
											<span class="product-new-top">New</span>
											
									</div>
									<div class="item-info-product ">
										<h4 class="ttl-prod"><a href="{{URL('/detail/'.$p2->id_produk )}}">{{ str_limit($p2->nama_produk, 20) }}</a></h4>
										<div class="info-product-price">
											<span class="item_price">Rp. {{ number_format($p2->harga, 2, ',', '.') }}</span>
											<!-- <del>$90.71</del> -->
										</div>
										<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
															<form action="#" method="post">
																<fieldset>
																	<input type="hidden" name="cmd" value="_cart" />
																	<input type="hidden" name="add" value="1" />
																	<input type="hidden" name="business" value=" " />
																	<input type="hidden" name="item_name" value="{{$p2->nama_produk}}" />
																	<input type="hidden" name="amount" value="{{$p2->harga}}" />
																	<input type="hidden" name="discount_amount" value="1.00" />
																	<input type="hidden" name="currency_code" value="USD" />
																	<input type="hidden" name="return" value=" " />
																	<input type="hidden" name="cancel_return" value=" " />
																	<input type="submit" name="submit" value="Add to cart" class="button" />
																</fieldset>
															</form>
														</div>
																			
									</div>
								</div>
							</div>
							@endforeach
							
					
							<div class="clearfix"></div>
						</div>
						<!--//tab_one-->
						<!--/tab_two-->
						<div class="tab2">
							@foreach($produk3 as $p3) 							
							<div class="col-md-3 product-men">
								<div class="men-pro-item simpleCart_shelfItem">
									<div class="men-thumb-item">
										<img src="{{$p3->image}}" alt="" class="pro-image-front">
										<img src="{{$p3->image}}" alt="" class="pro-image-back">
											<div class="men-cart-pro">
												<div class="inner-men-cart-pro">
													<a href="{{URL('/detail/'.$p3->id_produk )}}" class="link-product-add-cart">Quick View</a>
												</div>
											</div>
											<span class="product-new-top">New</span>
											
									</div>
									<div class="item-info-product ">
										<h4 class="ttl-prod"><a href="{{URL('/detail/'.$p3->id_produk )}}">{{ str_limit($p3->nama_produk, 20) }}</a></h4>
										<div class="info-product-price">
											<span class="item_price">Rp. {{ number_format($p3->harga, 2, ',', '.') }}</span>
											<!-- <del>$389.71</del> -->
										</div>
										<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
															<form action="#" method="post">
																<fieldset>
																	<input type="hidden" name="cmd" value="_cart" />
																	<input type="hidden" name="add" value="1" />
																	<input type="hidden" name="business" value=" " />
																	<input type="hidden" name="item_name" value="Pink Track Pants" />
																	<input type="hidden" name="amount" value="30.99" />
																	<input type="hidden" name="discount_amount" value="1.00" />
																	<input type="hidden" name="currency_code" value="USD" />
																	<input type="hidden" name="return" value=" " />
																	<input type="hidden" name="cancel_return" value=" " />
																	<input type="submit" name="submit" value="Add to cart" class="button" />
																</fieldset>
															</form>
														</div>
																			
									</div>
								</div>
							</div>
							@endforeach

							
						   
							<div class="clearfix"></div>
						</div>
					 <!--//tab_two-->
						<div class="tab3">
							@foreach($produk4 as $p4) 		
							<div class="col-md-3 product-men">
								<div class="men-pro-item simpleCart_shelfItem">
									<div class="men-thumb-item">
										<img src="{{$p4->image}}" alt="" class="pro-image-front">
										<img src="{{$p4->image}}" alt="" class="pro-image-back">
											<div class="men-cart-pro">
												<div class="inner-men-cart-pro">
													<a href="{{URL('/detail/'.$p4->id_produk )}}" class="link-product-add-cart">Quick View</a>
												</div>
											</div>
											<span class="product-new-top">New</span>
											
									</div>
									<div class="item-info-product ">
										<h4 class="ttl-prod"><a href="{{URL('/detail/'.$p4->id_produk )}}">{{ str_limit($p4->nama_produk, 20) }}</a></h4>
										<div class="info-product-price">
											<span class="item_price">Rp. {{ number_format($p4->harga, 2, ',', '.') }}</span>
											<!-- <del>$259.71</del> -->
										</div>
										<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
															<form action="#" method="post">
																<fieldset>
																	<input type="hidden" name="cmd" value="_cart" />
																	<input type="hidden" name="add" value="1" />
																	<input type="hidden" name="business" value=" " />
																	<input type="hidden" name="item_name" value=" Hand-held Bag " />
																	<input type="hidden" name="amount" value="30.99" />
																	<input type="hidden" name="discount_amount" value="1.00" />
																	<input type="hidden" name="currency_code" value="USD" />
																	<input type="hidden" name="return" value=" " />
																	<input type="hidden" name="cancel_return" value=" " />
																	<input type="submit" name="submit" value="Add to cart" class="button" />
																</fieldset>
															</form>
														</div>
																			
									</div>
								</div>
							</div>
							@endforeach
						
						
							<div class="clearfix"></div>
						</div>
						<div class="tab4">
							@foreach($produk5 as $p5) 	
							<div class="col-md-3 product-men">
								<div class="men-pro-item simpleCart_shelfItem">
									<div class="men-thumb-item">
										<img src="{{$p5->image}}" alt="" class="pro-image-front">
										<img src="{{$p5->image}}" alt="" class="pro-image-back">
											<div class="men-cart-pro">
												<div class="inner-men-cart-pro">
													<a href="{{URL('/detail/'.$p5->id_produk )}}" class="link-product-add-cart">Quick View</a>
												</div>
											</div>
											<span class="product-new-top">New</span>
											
									</div>
									<div class="item-info-product ">
										<h4 class="ttl-prod"><a href="{{URL('/detail/'.$p5->id_produk )}}">{{ str_limit($p5->nama_produk, 20) }}</a></h4>
										<div class="info-product-price">
											<span class="item_price">Rp. {{ number_format($p5->harga, 2, ',', '.') }}</span>
											<!-- <del>$99.71</del> -->
										</div>
										<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
															<form action="#" method="post">
																<fieldset>
																	<input type="hidden" name="cmd" value="_cart" />
																	<input type="hidden" name="add" value="1" />
																	<input type="hidden" name="business" value=" " />
																	<input type="hidden" name="item_name" value="Moonwalk Bellies" />
																	<input type="hidden" name="amount" value="30.99" />
																	<input type="hidden" name="discount_amount" value="1.00" />
																	<input type="hidden" name="currency_code" value="USD" />
																	<input type="hidden" name="return" value=" " />
																	<input type="hidden" name="cancel_return" value=" " />
																	<input type="submit" name="submit" value="Add to cart" class="button" />
																</fieldset>
															</form>
														</div>
																			
									</div>
								</div>
							</div>
							@endforeach
											
						<div class="clearfix"></div>
						</div>
					</div>	
			    </div>
		</div>